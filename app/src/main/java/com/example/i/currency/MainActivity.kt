package com.example.i.currency

import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.example.i.currency.adapter.CurrencyAdapter
import com.example.i.currency.pojo.Currency
import com.example.i.currency.pojo.Stock
import com.example.i.currency.utils.NetworkUtil
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import okhttp3.*
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {

    lateinit var currencyAdapter: CurrencyAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        // инициализируем адаптер
        currency_recycler_view.layoutManager = LinearLayoutManager(this)
        currencyAdapter = CurrencyAdapter()
        currency_recycler_view.adapter = currencyAdapter

        // получаем данные
        fetchJSON()


        val currencyTask = CurrencyTask()
        currencyTask.execute()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_update -> {
                fetchJSON()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    internal inner class CurrencyTask : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg params: Void): Void? {

            val timer = Timer()
            timer.schedule(object : TimerTask() {
                override fun run() {
                    try {
                        fetchJSON()
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }
                }
            }, 15000, 15000)

            return null
        }
    }

    fun fetchJSON(){
        val url = NetworkUtil().generateUrl()
        val client = OkHttpClient()

        val request = Request.Builder().url(url).build()

        client.newCall(request).enqueue(object : Callback{
            override fun onResponse(call: Call, response: Response) {
                val body = response.body()?.string()
                val gson = GsonBuilder().create()
                val stock = gson.fromJson(body, Stock::class.java)

                val currency: ArrayList<Currency> = arrayListOf()
                for (n in stock.stock){
                    currency.add(n)
                }

                // получаев время обновления
                val currentDate = Date()
                val dateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault())
                val dateText = dateFormat.format(currentDate)

                // обновляем данные в адапторе и добавляем дату обновления
                runOnUiThread{
                    currencyAdapter.clearItems()
                    currencyAdapter.setItems(currency)
                    updateDateTextView.text = dateText
                }
            }
            override fun onFailure(call: Call, e: IOException) {
                println("Ошибка при выполнении запроса: $e")
            }
        })
    }

}


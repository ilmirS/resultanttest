package com.example.i.currency.utils

import android.net.Uri
import java.net.URL

class NetworkUtil {

    private val BASE_URL = "http://phisix-api3.appspot.com"
    private val CURRENCY_GET = "/stocks.json"

    // генерируем url
    fun generateUrl(): URL {
        val buildUrl: Uri = Uri.parse(BASE_URL + CURRENCY_GET)
        return URL(buildUrl.toString())
    }

}

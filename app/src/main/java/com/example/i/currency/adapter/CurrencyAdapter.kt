package com.example.i.currency.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.i.currency.R
import com.example.i.currency.pojo.Currency
import kotlinx.android.synthetic.main.currency_item_view.view.*

class CurrencyAdapter : RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder>() {


    private var currencyList: ArrayList<Currency> = ArrayList()

    inner class CurrencyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun bind(currency: Currency){
            itemView.nameTextView.text = currency.name
            itemView.priceTextView.text = currency.volume.toString()
            itemView.amountTextView.text = currency.price.amount.toString()
        }
    }

    fun setItems(currencies: ArrayList<Currency>){
        currencyList.addAll(currencies)
        notifyDataSetChanged()
    }
    fun clearItems(){
        currencyList.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): CurrencyViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.currency_item_view, parent, false)
        return CurrencyViewHolder(view)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(currencyList[position])
    }

    override fun getItemCount(): Int {
       return currencyList.size
    }
}


package com.example.i.currency.pojo

data class Stock(val stock: List<Currency>)

data class Currency(val name: String, val price: Price, val percent_change: Double, val volume: Int, val symbol: String)

data class Price(val currency: String, val amount: Double)